package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	containerUpMetric = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "docker_container_up",
			Help: "Flag indicating if the docker container is in the \"running\" state",
		},
		[]string{"name", "image_name", "image_version"})
	containerStartTimestampMetric = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "docker_container_start_timestamp",
			Help: "Unix timestamp of container last start or restart",
		},
		[]string{"name"})
	containerRestartCountMetric = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "docker_container_restart_number_total",
			Help: "Number of container restarts (the real metric tupe is 'counter')",
		},
		[]string{"name"})
	promRegistry *prometheus.Registry
	promClient   *client.Client
)

func main() {
	promRegistry = prometheus.NewRegistry()
	promRegistry.MustRegister(containerUpMetric)
	promRegistry.MustRegister(containerStartTimestampMetric)
	promRegistry.MustRegister(containerRestartCountMetric)
	client, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}
	promClient = client

	http.HandleFunc("/", getRoot)
	http.HandleFunc("/metrics", getMetrics)
	http.ListenAndServe(":9777", nil)
}
func getRoot(w http.ResponseWriter, r *http.Request) {
	response_body := fmt.Sprintf(
		"<html><body>"+
			"This is a simple docker exporter that exposes some metrics of the docker objects.</br>"+
			"Check out the <a href=\"http://%s/metrics\">/metrics</a> page"+
			"</body></html>",
		r.Host)
	io.WriteString(w, response_body)
}
func getMetrics(w http.ResponseWriter, r *http.Request) {
	updateMetrics()
	promhttp.HandlerFor(promRegistry, promhttp.HandlerOpts{}).ServeHTTP(w, r)
}

func updateMetrics() {
	containers, err := promClient.ContainerList(context.Background(), types.ContainerListOptions{All: true})
	if err != nil {
		panic(err)
	}

	for _, container := range containers {
		image := strings.Split(container.Image, ":") // name starts with the "/" symbol, it's trimmed with [1:] slicing
		name := container.Names[0][1:]
		image_name := image[0]

		var image_version string
		if len(image) == 1 {
			image_version = "latest"
		} else {
			image_version = image[1]
		}

		if container.State == "running" {
			containerUpMetric.WithLabelValues(name, image_name, image_version).Set(1)
		} else {
			containerUpMetric.WithLabelValues(name, image_name, image_version).Set(0)
		}

		container_details, err := promClient.ContainerInspect(context.Background(), container.ID)
		if err != nil {
			panic(err)
		}
		t, err := time.Parse(time.RFC3339, container_details.State.StartedAt)
		if err != nil {
			panic(err)
		}
		ut := t.UnixNano() / int64(time.Millisecond)

		containerStartTimestampMetric.WithLabelValues(name).Set(float64(ut))
		containerRestartCountMetric.WithLabelValues(name).Set(float64(container_details.RestartCount))
	}
}
