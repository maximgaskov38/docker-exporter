FROM golang:1.21.0

WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY *.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -o /docker-exporter
EXPOSE 9777

CMD ["/docker-exporter"]
